
//
//  IAPManager.swift
//  TestInappPurchase
//
//  Created by Hira on 1/18/18.
//  Copyright © 2018 Hira. All rights reserved.
//

import Foundation
import SwiftyStoreKit
import StoreKit

let kProduct = "com.official.astrology.askquestion" //Product ID
let kusername = ""

//let kUserName = ""
//let receiptData = SwiftyStoreKit.localReceiptData
//let receiptString = receiptData?.base64EncodedString(options: [])
//

class IAPManager: NSObject {
    
    static func getProductInfo() {

        SwiftyStoreKit.retrieveProductsInfo([kProduct], completion: { result in

            if result.error == nil {

                if result.retrievedProducts.count > 0 {
                    purchase(complition: { receipt,error  in
                        print(receipt)
                    })
                }
            }
        })
    }
    
    class func purchase(complition:  @escaping (String?,String?) -> Void) {

        SwiftyStoreKit.purchaseProduct(kProduct, quantity: 1, atomically: true, applicationUsername: kusername, completion: {result in
            switch result {
            case .success(purchase: let product):
                //                product.
                refreshReceipt(complation: { result in
                    
                    if result != nil {
                        complition((result?.base64EncodedString())!,nil)
                    }else {
                        complition(nil,"No receipt found.")
                    }
                })
                
            case .error(error: let error):
                switch error.code {
                case .clientInvalid:
                    print("invalide user")
                    complition(nil,"invalide user")
                    
                case .paymentCancelled:
                    print("Cancelled")
                    complition(nil,"Cancelled")
                    
                case .paymentNotAllowed:
                    print("payment not allowed")
                    complition(nil,"payment not allowed")
                    
                case .storeProductNotAvailable:
                    print("product not available")
                    complition(nil,"Product not available.")
                    
                default:
                    print("other error")
                    complition(nil,"Unknown error. Please try later.")
                }
            }
        })
    }
    
    
    class func restorePurchase(applicationUsername : String, completion: @escaping ((String)?) ->()) {
        
        SwiftyStoreKit.restorePurchases(atomically: true, applicationUsername: applicationUsername, completion: {result in
//            let transactions = result.restoredPurchases
            //            for transaction in transactions {
            //
            //                switch transaction.transaction.transactionState {
            //                case .purchased:
            //                    print("Product Purchased")
            //                    break
            //
            //                case .failed:
            //                    print("Purchased Failed")
            //                    break
            //
            //                case .restored:
            //                    print("Already Purchased")
            ////                    refreshReceipt(complation: {receipt in
            ////                        if receipt != nil {
            ////                            completion(receipt?.base64EncodedString())
            ////                        }
            ////                    })
            //
            //                default:
            //                    break
            //                }
            //                break
            //            }
            //
            refreshReceipt(complation: {receipt in
                if receipt != nil {
                    completion(receipt?.base64EncodedString())
                }else {
                    completion(nil)
                }
            })
        })
    }
    
    class func refreshReceipt(complation: @escaping (Data?) -> Void) {
        
        SwiftyStoreKit.fetchReceipt(forceRefresh: false, completion: {result in
            
//            debugPrint(result)
            switch result {
                
            case .success(receiptData: let receipt):
                
        //          receipt.base64EncodedString()
                complation(receipt)
                
            case .error(error: let error):
                print(error)
                complation(nil)
            }
            
        })
        
    }
    
    
    class func verifyReceipt(receipt : ReceiptValidator) {
        
        SwiftyStoreKit.verifyReceipt(using: receipt, completion: {
            result in
            
            switch result {
                
            case .success(receipt: let receiptinfo):
                
                verifyPurchase(receiptinfo: receiptinfo)
                print(receipt)
                
            case.error(error: let error):
                print(error.localizedDescription)
            }
            
        })
    }
    
    class func verifyPurchase(receiptinfo: ReceiptInfo) {
        
        let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: kProduct, inReceipt: receiptinfo)
        
        switch purchaseResult {
        case .notPurchased:
            print("Could not purchase")
            
        case .purchased(let receiptItem):
            print(receiptItem)
            
        }
    }
    
    static func verifyStoreIAP(base64String:AnyObject, comletionHandler:@escaping(([[String:AnyObject]]?,_ errorType:String) -> ())){
        let params = ["receipt-data":base64String as AnyObject ,"password":"" as AnyObject]
        let url = URL(string:"https://sandbox.itunes.apple.com/verifyReceipt")!
                
//        ApiManager.getData(url: url, method: .post, parameters: params,headers:nil, onCompletion: {
//            response in
//            debugPrint(response)
//            switch response.result {
//            case .success( let sucData):
//                debugPrint(sucData)
//                if let data = sucData as? [String:AnyObject]{
//                    if let receipt = data["receipt"] as? [String:AnyObject] {
//                        if let inApp = receipt["in_app"] as? [[String: AnyObject]] {
//                            comletionHandler(inApp, "")
//                        }
//                    }
//                }
//                else{
//                    comletionHandler(nil,"No data in receipt.")
//                }
//                
//            case .failure(let error):
//                print(error.localizedDescription)
//                comletionHandler(nil,error.localizedDescription)
//            }
//        })
    }
}

