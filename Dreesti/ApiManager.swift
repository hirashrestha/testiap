//
//  ApiManager.swift
//  Dreesti
//
//  Created by Hira Shrestha on 3/7/18.
//  Copyright © 2018 Insight Workshop. All rights reserved.
//

import Foundation
import  Alamofire

class ApiManager {
    
    static func getData(url: String, paramaters: [String : String]?, headers: [String : String]?, sucess: @escaping([String : AnyObject]) -> (), failure: @escaping(String) ->()) {
        
        Alamofire.request(url, method: .post, parameters: paramaters, headers : headers).responseJSON {response in
            
            switch response.result {
                
            case .success(let responseObject):
                
                switch response.response?.statusCode {
                    
                case 200?:
                    
                    if let data = responseObject as? [String : AnyObject] {
                        
                        sucess(data)
                    }
                    
                default:
                    
                    failure("data is nil")
                }
                
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
}
