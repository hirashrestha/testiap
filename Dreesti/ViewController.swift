//
//  ViewController.swift
//  Dreesti
//
//  Created by Insight Workshop on 3/6/18.
//  Copyright © 2018 Insight Workshop. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func makePurchase(_ sender: UIButton) {
        
        IAPManager.getProductInfo()
//        makePurchase()
//
//        SwiftyStoreKit.retrieveProductsInfo(["com.official.astrology.askquestion"]) { result in
//            if let product = result.retrievedProducts.first {
//                let priceString = product.localizedPrice!
//                print("Product: \(product.localizedDescription), price: \(priceString)")
//            }
//            else if let invalidProductId = result.invalidProductIDs.first {
//                print("Invalid product identifier: \(invalidProductId)")
//            }
//            else {
//                print("Error: \(result.error)")
//            }
//        }
        
    }
    
    func makePurchase() {
        
        IAPManager.purchase(complition: { receipt,error in
            if error == nil {
                if let receipt = receipt {
                    
                    print(receipt)
                }
            }
        })
    }
    
}

